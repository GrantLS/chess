var Piece = require('./piece');

function Bishop(player) {
    Piece.call(this, player);
}

Bishop.prototype = Object.create(Piece.prototype);
Bishop.prototype.constructor = Bishop;

module.exports = Bishop;