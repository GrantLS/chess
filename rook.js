var Piece = require('./piece');

function Rook(player) {
    Piece.call(this, player);
}

Rook.prototype = Object.create(Piece.prototype);
Rook.prototype.constructor = Rook;

module.exports = Rook;