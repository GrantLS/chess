var Piece = require('./piece');

function Knight(player) {
    Piece.call(this, player);
}

Knight.prototype = Object.create(Piece.prototype);
Knight.prototype.constructor = Knight;

module.exports = Knight;