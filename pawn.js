var Piece = require('./piece');

function Pawn(player) {
    Piece.call(this, player);
}

Pawn.prototype = Object.create(Piece.prototype);
Pawn.prototype.constructor = Pawn;

module.exports = Pawn;