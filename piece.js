var {LOWEST_FILE, LOWEST_RANK, HIGHEST_FILE, HIGHEST_RANK} = require('./constants');

function Piece(player) {
	this.player = player;
    this.moveCount = 0;
    this.board;
};

Piece.prototype.getPlayer = function getPlayer(){
	return this.player;
}

Piece.prototype.getBoard = function getBoard(){
    return this.board;
}

Piece.prototype.setBoard = function setBoard(board){
    this.board = board;
}

Piece.prototype.incrementMoveCounter = function incrementMoveCounter(){
    this.moveCount++;
}

Piece.prototype.startsAndEndsOnBoard = function startsAndEndsOnBoard(x1, y1, x2, y2){
	return !(x1 < LOWEST_FILE || x1 > HIGHEST_FILE || x2 < LOWEST_FILE || x2 > HIGHEST_FILE || y1 < LOWEST_RANK || y1 > HIGHEST_RANK || y2 < LOWEST_RANK || y2 > HIGHEST_RANK);
}

Piece.prototype.notSamePlayerAtDestination = function notSamePlayerAtDestination(x1, y1, x2, y2){
    console.log(this.board);
    var player = this.board.getPlayerAtSquare(y2, x2);
    console.log(player);
    return player != this.player;
}

Piece.prototype.validateMove = function validateMove(x1, y1, x2, y2){
	var rv = true;
  if(!this.startsAndEndsOnBoard(x1, y1, x2, y2)){
  	rv = false;
  }else if(!this.notSamePlayerAtDestination(x1, y1, x2, y2)){
  	rv = false;
  }
  
  return rv;
}

module.exports = Piece;