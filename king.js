var Piece = require('./piece');

function King(player) {
     Piece.call(this, player);
}

King.prototype = Object.create(Piece.prototype);
King.prototype.constructor = King;

module.exports = King;