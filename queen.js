var Piece = require('./piece');

function Queen(player) {
     Piece.call(this, player);
}

Queen.prototype = Object.create(Piece.prototype);
Queen.prototype.constructor = Queen;

module.exports = Queen;