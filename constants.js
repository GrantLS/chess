module.exports = {
    LOWEST_RANK: 1,
    HIGHEST_RANK: 8,
    LOWEST_FILE: 1,
    HIGHEST_FILE: 8,
    WHITE_PLAYER: 1,
    BLACK_PLAYER: 2
}