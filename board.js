var Empty = require('./empty');
var Piece = require('./piece');

function Board(){
	this.squares = [
  	[],
    [],
    [],
    [],
    [],
    [],
    [],
    []
  ];

}

Board.prototype.setPiece = function setPiece(rank, file, piece){
    if(!(piece instanceof Empty))
        piece.setBoard(this);
      
    this.squares[rank - 1][file - 1] = piece;
}

Board.prototype.getPiece = function getPiece(rank, file){
	return this.squares[rank - 1][file - 1];
}

Board.prototype.clearSpace = function clearSpace(rank, file){
    this.squares[rank - 1][file - 1] = new Empty;
}

Board.prototype.movePiece = function movePiece(rank1, file1, rank2, file2){
    //Will need to validate move
    var piece = this.getPiece(rank1, file1);
    if(piece.validateMove(file1, rank1, file2, rank2)){
        piece.incrementMoveCounter();
        this.setPiece(rank2, file2, piece);
        this.clearSpace(rank1, file1);
        return true;
    }else{
        return false;
    }
}

Board.prototype.getPlayerAtSquare = function getPlayerAtSquare(rank, file){
    if(!(piece instanceof Empty))
    var piece = this.getPiece(rank, file);

    if(!(piece instanceof Empty))
        return piece.getPlayer();
    else
        return 0;
}

module.exports = Board;