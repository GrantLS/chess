var {WHITE_PLAYER, BLACK_PLAYER, LOWEST_FILE, LOWEST_RANK, HIGHEST_FILE, HIGHEST_RANK} = require('./constants');
var King = require('./king');
var Queen = require('./queen');
var Bishop = require('./bishop');
var Knight = require('./knight');
var Rook = require('./rook');
var Pawn = require('./pawn');

var Board = require('./board');

var Empty = require('./empty');

var board = new Board(); 
board.setPiece(1,1, new Rook(WHITE_PLAYER));
board.setPiece(1,2, new Knight(WHITE_PLAYER));
board.setPiece(1,3, new Bishop(WHITE_PLAYER));
board.setPiece(1,4, new Queen(WHITE_PLAYER));
board.setPiece(1,5, new King(WHITE_PLAYER));
board.setPiece(1,6, new Bishop(WHITE_PLAYER));
board.setPiece(1,7, new Knight(WHITE_PLAYER));
board.setPiece(1,8, new Rook(WHITE_PLAYER));

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(2,i,new Pawn(WHITE_PLAYER));

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(3,i,new Empty());

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(4,i,new Empty());

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(5,i,new Empty());

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(6,i,new Empty());

for(i = LOWEST_FILE; i <= HIGHEST_FILE; i++)
    board.setPiece(7,i,new Pawn(BLACK_PLAYER));
    
board.setPiece(8,1, new Rook(BLACK_PLAYER));
board.setPiece(8,2, new Knight(BLACK_PLAYER));
board.setPiece(8,3, new Bishop(BLACK_PLAYER));
board.setPiece(8,4, new Queen(BLACK_PLAYER));
board.setPiece(8,5, new King(BLACK_PLAYER));
board.setPiece(8,6, new Bishop(BLACK_PLAYER));
board.setPiece(8,7, new Knight(BLACK_PLAYER));
board.setPiece(8,8, new Rook(BLACK_PLAYER));
  
// console.log(board.getPiece(8,8));
// board.movePiece(8,8,4,4);
// console.log(board.getPiece(8,8));
// console.log(board.getPiece(4,4));

// path.find = function find(x1, y1, x2, y2){
//   var e = "Unknown type of move";
//   var slope = this.slope(x1, y1, x2, y2);
//   var rv = [];
//   //console.log(slope);
  
//   switch(slope){
//     //Horizontal lines
//     case 0:
//     case -0:
//       for(var i = 0; Math.abs(i) <= Math.abs(x2 - x1); i += (x2 - x1)/Math.abs(x2 - x1))
//         rv.push({"x": x1 + i, "y": y1});
//       break;
    
//     //Vertical lines
//     case Infinity:
//     case -Infinity:
//       for(var i = 0; Math.abs(i) <= Math.abs(y2 - y1); i += (y2 - y1)/Math.abs(y2 - y1))
//         rv.push({"x": x1, "y": y1 + i});
//       break;
      
//     //Diagonal lines
//     case 1:
//     	for(var i = 0, j = 0; Math.abs(i) <= Math.abs(x1 - x2); i += (x2 - x1)/Math.abs(x2 - x1), j = i)
//       rv.push({"x": x1 + i, "y": y1 + j})
//     	break;
//     case -1:
//     	for(var i = 0, j = 0; Math.abs(i) <= Math.abs(x1 - x2); i += (x2 - x1)/Math.abs(x2 - x1), j += (x1 - x2)/Math.abs(x1 - x2))
//       rv.push({"x": x1 + i, "y": y1 + j})
//     	break;
  	
//     //Knight - remember to check for length of the move
//     case 1/2:
//     case -1/2:
//       console.log(Math.abs(x2 - x1));
//     	if(Math.abs(x2 - x1) > 2)
//       	throw e;
//       break;
//     case 2:
//     case -2:
//     	//console.log("here");
//     	if(Math.abs(y2 - y1) > 2)
//       	throw e;
//       break;
  
//   	default:
//     	throw e;
//     	break;
//   }
  
//   return rv;
// }

// path.slope = function slope(x1, y1, x2, y2){
//   return (y2 - y1)/(x2 - x1);
// }

// path.isBlocked = function isBlocked(currentBoard, x1, x2, y1, y2){
//   var myPath = this.find(x1, x2, y1, y2); 
//   var rv = false;
//   //We need to remove the first and last squares because they don't
//   //represent blocks
//   myPath.splice(0,1);
//   myPath.splice(myPath.length - 1, 1)
  
//   myPath.map(function(a, i){
//     if(currentBoard[a.y - 1][a.x - 1] != EMPTY_SPACE)
//     	rv = true;
//   });
  
//   return rv;
// }

// path.hasEmptyDestination = function hasEmptyDestination(currentBoard, x2, y2){
// 	return currentBoard[y2 - 1][x2 - 1] == EMPTY_SPACE;
// }

// path.hasEmptySource = function hasEmptySource(currentBoard, x1, y1){
// 	return currentBoard[y1 - 1][x1 - 1] == EMPTY_SPACE;
// }

// /*console.log(path.find(1,1,8,1));
// console.log(path.find(8,1,1,1));
// console.log(path.find(1,1,1,8));
// console.log(path.find(1,8,1,1));
// console.log(path.find(1,1,8,8));
// console.log(path.find(8,8,1,1));
// console.log(path.find(1,8,8,1));
// console.log(path.find(8,1,1,8));
// console.log(path.find(1,1,2,3));
// console.log(path.find(1,1,3,2));*/
// /*try{
// 	console.log(path.find(1,1,5,3));
// }catch (e){
// 	console.log(e + " reported");
// }
// try{
// 	console.log(path.find(1,1,3,5));
// }catch (e){
// 	console.log(e + " reported");
// }*/
// /*
// console.log(path.find(2,3,1,1));
// console.log(path.find(3,2,1,1));
// console.log(path.find(2,1,1,3));
// try{
// 	console.log(path.find(-1,9,-1,9));
// }catch (e){
// 	console.log(e + " reported");
// }
// try{
// 	console.log(path.find(1,1,2,8));
// }catch (e){
// 	console.log(e + " reported");
// }*/

// /*console.log(path.isBlocked(myBoard, 1,1,8,8));
// console.log(path.isBlocked(myBoard, 3,3,5,5));
// console.log(path.isBlocked(myBoard, 1,1,2,2));*/

// //console.log(path.hasEmptyDestination(myBoard, 1,1));
// //console.log(path.hasEmptyDestination(myBoard, 3,3));
// //console.log(bishop.validateMove(myBoard, currentPlayer, 1,1,8,8));
// //console.log(board.getPlayerAtSquare(myBoard, 8,8))
// //console.log(path);
